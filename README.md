# Windsor

I wrote this cover letter as a way to learn more about Backbone and to serve as a practical demonstration of my coding style and experience. 

Its written to match many components of Windsor Circle's tech stack as described in the job listings, though I did not adhere too strictly to that.

The technologies used:

- I used Yeoman to generate the application structure.  As a new Backbone user it was nice to lean on its conventions and project structure.

- The JS code is written in Coffeescript.  It's my preferred way of writing Javascript, and it seemed appropriate since it was created by the author of Backbone and the significant whitespace should feel comfortable to Python developers.

- The CSS is written using LESS.

- Backbone, jQuery, Handlebars and RequireJS are all included.  This is a bit of overengineering for a simple cover letter, but the intent was to learn about building a Backbone app. 

If you wish to run the project locally, you can clone this repository and then execute

    npm install
    bower install
    grunt server

 You will need to have Node.js, npm, bower and grunt preinstalled.  If you have difficulties with this feel free to email me or open an issue in this repository.
 
 Otherwise, feel free to look through the source here on BitBucket.
