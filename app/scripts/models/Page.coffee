define [
  'underscore'
  'backbone'
], (_, Backbone) ->
  'use strict';

  # Define a model for each page.  Really just a skeleton for now
  class PageModel extends Backbone.Model
    defaults:
        name: "Default"
        file: "hello.html"
        selected: false

    # set the selected state of this page
    setSelected: (@selected) -> undefined

    isSelected: -> @selected