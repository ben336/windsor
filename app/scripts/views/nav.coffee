# Set up a view that defines the behavior of the navigation links

define [
  'jquery'
  'underscore'
  'backbone'
  'templates'
], ($, _, Backbone, JST) ->

  class NavView extends Backbone.View

    el: '#nav'

    template: JST['app/scripts/templates/nav.hbs']

    initialize: ()-> 
        _.bindAll @, "render"
        @collection.bind "change", @render
        @render()
        
    render: ()-> 
        sidx = @collection.getSelectedIdx()
        len = @collection.length
        prev = if sidx > 0 then @collection.at(sidx - 1) else null
        next = if sidx < len + 1 then @collection.at(sidx + 1) else null
        @$el.html this.template 
            prev: if prev? then prev.toJSON() else null 
            next: if next? then next.toJSON() else null 