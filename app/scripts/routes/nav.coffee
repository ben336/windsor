# This is the main Router class.  It matches pages to their corresponding HTML file
define [
  'jquery'
  'backbone'
], ($,Backbone) ->

  class NavRouter extends Backbone.Router

    initialize: (@pages) -> undefined #set pages as a property on the class

    routes: 
        "page/:p" : "page"
        "*path" : "default"

    page: (pagename)->
        currentPage = (@pages.filter (page) -> page.get("file") is pagename)[0]
        @pages.setSelected currentPage
        $(".content").load "scripts/templates/#{pagename}.html", ->
            $(".content *").hide()
            $(".content *").fadeIn(300)      
            
    default: () -> @page("hello")