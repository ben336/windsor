# Hello Windsor Circle

My name is Ben McCormick.  I'm a Software Engineer with a focus on 
front end web technologies, and I would love to help you make Windsor Circle's web application more usable, useful and functional.  
I've spent the last 3 years solving data filtering and visualization problems in the context of the browser while working on an enterprise-scale performance management platform.  I want to use that experience to benefit Windsor Circle and your customers by making it easy to work with their data and surface the insights that your back-end code provides in a beautiful and intuitive fashion.  

Along with experience in base web technologies like JavaScript, CSS, and HTML, I'm very familiar with the wider web development ecosystem.  I have significant experience using JavaScript MVC technologies like [KnockoutJS][knockout] and [AngularJS][angular], as well as CSS preprocessors like [LESS][less] and [Stylus][stylus]. On a day to day basis I often use [Knockout][knockout], [jQuery][jquery], [RequireJS][require], and [Highcharts][highcharts] to create charts and tables for data display and analysis. I also pick up new technologies quickly; [I wrote this page][bitbucket] as a way to learn about Backbone.

[jquery]:http://jquery.com/
[knockout]:http://knockoutjs.com
[require]:http://requirejs.org/
[highcharts]: http://highcharts.com/
[angular]: http://angularjs.org/
[less]:http://lesscss.org/
[stylus]: https://github.com/learnboost/stylus
[bitbucket]: https://bitbucket.org/ben336/windsor