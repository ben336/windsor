# Et cetera

A few more assorted facts about me:

- I live with my wife Claire in South Durham near the American Tobacco Trail.
- I enjoy writing, and usually write about technology a few times a month at my personal blog [benmccormick.org](http://benmccormick.org).
- I like to code as a hobby, but I also love playing Basketball, Ultimate Frisbee, and board games.
- I prefer standing desks and [ergonomic keyboards](http://www.microsoft.com/hardware/en-us/p/natural-ergonomic-keyboard-4000).  I love what I do and want to make sure I can do it in a sustainable way.
- I'm a huge fan of the Pittsburgh Steelers and Duke Basketball.
- I'm a [regular contributor on Stack Overflow](http://stackoverflow.com/users/1424361/ben-mccormick).  
- I'm a member of the [Summit Church](http://www.summitrdu.com/) in Durham, and serve as the house manager of their [West Club Blvd location](http://www.summitrdu.com/campus/west-club/).

<div class="btn-container">
[<div class="btn">Interested? Let's Talk</div>](mailto:ben.mccormick@alumni.duke.edu) 
</div>