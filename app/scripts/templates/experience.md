# My Experience

This is a brief summary of my recent experience.  You can see my resume [here](http://benmccormick.org/resume.pdf).

### Cisco Systems - Software Engineer II (2011- Present)

- Worked as a front-end developer on Prime Performance Manager, an enterprise performance management application targeted at service providers such as Verizon and AT&T
- Implemented features related to data visualization and filtering using a mix of JavaScript and Java
- Everyday use of [jQuery][jquery], [KnockoutJS][ko], [RequireJS][require], and [Highcharts][highcharts]

### Software Development Institute - Software Developer (2010-2011)

- Worked on a network security web application as part of a 3 member team
- Developed features in Java and designed the application's database schema

### Carnegie Mellon - MS in Information Systems Management (2010-2011)

### Duke University - BS in Computer Science (2006-2010)


[jquery]:http://jquery.com/
[ko]:http://knockoutjs.com
[require]:http://requirejs.org/
[highcharts]: http://highcharts.com/