# I want to help!

As a front end developer, I would bring the following skills to Windsor Circle:

- JavaScript Expertise

    I have very strong knowledge of the base JavaScript language, along with extensive experience with the JavaScript ecosystem, including [jQuery][jquery], [RequireJS][require], and MVC frameworks.

- Communication Skills
    
    I pride myself on the ability to talk through problems and get to the root issues. I'm comfortable working through a problem with people on a whiteboard, or troubleshooting over IM when team members are working remotely.


- Data Visualization Experience
    
    I've spent the last 3 years working on data visualization and manipulation. That's given me plenty of opportunity to experiment with what works and what doesn't, and I would love to put that experience to work for Windsor Circle.

[jquery]:http://jquery.com/
[knockout]:http://knockoutjs.com
[require]:http://requirejs.org/
[highcharts]: http://highcharts.com/
[angular]: http://angularjs.org/
[less]:http://lesscss.org/
[stylus]: https://github.com/learnboost/stylus
[bitbucket]: https://bitbucket.org/ben336/windsor
