# This is the main application file
# We configure RequireJS then initalize the Router, page Collection and View
'use strict'

require.config
  baseUrl:'scripts'
  shim:
    underscore:
      exports: '_'
    backbone:
      deps: [
        'underscore'
        'jquery'
      ]
      exports: 'Backbone'
    handlebars:
      exports: 'Handlebars'
  paths:
    jquery: '../bower_components/jquery/jquery'
    backbone: '../bower_components/backbone/backbone'
    underscore: '../bower_components/underscore/underscore'
    handlebars: '../bower_components/handlebars/handlebars'

require [
  'backbone'
  'models/Page'
  'collections/pages'
  'views/nav'
  'routes/nav'
], (Backbone,PageModel,PagesCollection,NavView,NavRouter) ->

  # initialize a few pages and add them to a collection
  pages = new PagesCollection
  pages.add new PageModel {name:"Hello",file:"hello"}
  pages.add new PageModel {name:"How I Can Help",file:"help"}
  pages.add new PageModel {name:"Experience",file:"experience"}
  pages.add new PageModel {name:"Etc",file:"etc"}

  app = new NavRouter pages
  Backbone.history.start()
  
  # Initialize a new navigation view and add the pages collection
  new NavView {collection:pages}

