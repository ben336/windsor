define [
  'underscore'
  'backbone'
  'models/Page'
], (_, Backbone, PageModel) ->

  class PagesCollection extends Backbone.Collection
    model: PageModel

    #set a page selected given the page object
    setSelected : (selectedpage) ->
        idx = @.indexOf selectedpage
        @.forEach (page) ->
            page.setSelected(page is selectedpage)
        #necessary to make sure the view picks up on the change
        @trigger "change"

    #get the index of the selected page
    getSelectedIdx: ->
        selected = @.find (page) -> page.isSelected()
        idx = @.indexOf selected


